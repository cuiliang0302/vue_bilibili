import Vue from 'vue'
import { Button, Field, Toast, Form, Icon, Image, Grid, GridItem, Tab, Tabs, Uploader, Dialog, ActionSheet } from 'vant'

Vue.use(Button)
Vue.use(Field)
Vue.prototype.$msg = Toast
Vue.use(Form)
Vue.use(Icon)
Vue.use(Image)
Vue.use(GridItem)
Vue.use(Grid)
Vue.use(Tab)
Vue.use(Tabs)
Vue.use(Uploader)
Vue.use(Dialog)
Vue.use(ActionSheet)
