import axios from 'axios'

const http = axios.create({
  baseURL: 'http://112.74.99.5:3000/web/api'
})
// 请求时头部携带token认证信息
http.interceptors.request.use(config => {
  if (window.sessionStorage.getItem('token')) {
    config.headers.Authorization = 'Bearer ' + window.sessionStorage.getItem('token')
  }
  return config
})
export default http
